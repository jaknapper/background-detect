# -*- coding: utf-8 -*-
"""
Reconstruction of tiled images

@author: rwb27
"""

import numpy as np
import matplotlib.pyplot as plt
import cv2
import os
import glob
import json
import piexif
import logging
from datetime import datetime
import pickle 

from sklearn import linear_model
from copy import deepcopy

from alive_progress import alive_bar
import time

def get_capture_time(link):
    try:
        image_time = pull_usercomment_dict(link)['image']['time']
        image_time = datetime.strptime(image_time, '%Y-%m-%dT%H:%M:%S.%f')
    except:
        print(link)

    return image_time

def get_pixel_positions(dir, img_list, px_per_step, resize, img_limit = 0):
    """Given a list of images, extract the relative positions of the images.
    
    tile_datasets: [h5py.Dataset]
        Each tile should be an image, assumed to be all the same size, in RGB
        format.  Each must have attributes "camera_centre_position" (which is
        the position of the centre of the image in sample coordinates, i.e.
        microns).
    camera_to_sample: numpy.ndarray
        The camera_to_sample matrix maps displacement in camera units (where 
        the width or height of an image is 1) into sample units (generally
        corresponding to actual distance)
        
    Returns: numpy.ndarray, numpy.ndarray
        The first return value is Nx2, where N is the number of tiles.  The 
        positions are relative to the mean position of all images, and are in 
        units of pixels (NB NOT "camera units").
        The second return value is 2 elements, the mean position (in sample
        coordinates) of all the images.  This corresponds to a "pixel position"
        of (0,0)
    """
    # img_list = [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir,f)) and f.lower().endswith(('.png', '.jpg', '.jpeg', '.tiff', '.bmp', '.gif')) and "stitched" not in f]
    img_list.sort(key=lambda x: get_capture_time(os.path.join(dir,x)))

    if img_limit !=0:
        img_list = img_list[:img_limit]

    images = []
    positions = [] 
    pixel_positions = []

    for link in img_list:
        link = os.path.join(dir, link)
        data = pull_usercomment_dict(link)

        x_stage, y_stage, z_stage = (data['instrument']['state']['stage']['position'].values())

        images.append(link)
        positions.append([x_stage, y_stage])
    
    img = cv2.imread(images[0], 0)

    centre = np.mean(positions, axis=0)[:2]

    positions = np.array(positions) - centre[:2]

    pixel_positions = resize * np.dot(positions[:,:2], 
                             np.linalg.inv(px_per_step)) * img.shape[:2] / np.asarray([624, 832])
    
    image, _, _ = stitch_images(images, pixel_positions / resize, downsample= 9)

    #TODO: make downsample here depend on the size and number of images?

    cv2.imwrite("pixels_image.jpg", cv2.cvtColor(image[:,:,::-1], cv2.COLOR_BGR2RGB))
    # plot_images_in_situ(images, pixel_positions)
    return images, pixel_positions, centre, positions

def plot_images_in_situ(tiles, positions, ax=None, 
                        downsample=5, outlines=False, centres=False):
    """Plot a set of images at the given (pixel) positions.
    
    Arguments:
    tiles: [h5py.Dataset]
        A list of images as HDF5 datasets or numpy ndarrays.
    positions: numpy.ndarray Nx2
        A numpy array of positions, in pixels, of each image.
    ax: matplotlib.Axes
        An axes object in which to plot the images - default: create new figure
    downsample: int
        Set the downsampling factor: we speed up plotting by only using every
        nth pixel in X and Y.  Default may vary - but currently it's 5.
    outlines: bool
        Whether to plot the outline of each image as a line
    centres: bool
        Whether to plot a dot at the centre of each image
    """
    # if ax is None:
    #     f, ax = plt.subplots(1,1)
    # ax.set_aspect(1)

    # #plot the images in position
    # for i, pos in enumerate(positions):
    #     image = cv2.imread(tiles[i], -1)[...,::-1].copy()
    #     small_image = image[::downsample,::downsample,:]
    #     ax.imshow(small_image[::1,::-1,:].transpose(1,0,2),
    #               extent=[pos[i] + s*image.shape[i] 
    #                       for i in [0,1] for s in [-0.5,0.5]],
    #              )
    # if outlines:#plot the outlines
    #     square = np.array([[-1,-1],[-1,1],[1,1],[1,-1],[-1,-1]])
    #     for pos in positions:
    #         rect = pos + np.array(image.shape[:2]) * 0.5 * square
    #         ax.plot(rect[:,0],rect[:,1])
    # if centres:#plot the centre of each image
    #     for i, pos in enumerate(positions):
    #         ax.text(pos[0],pos[1],i)
    # ax.autoscale()
    # plt.savefig('situ.png')
    for i, pos in enumerate(positions):
        image = cv2.imread(tiles[i], -1)[...,::-1].copy()
        small_image = image[::downsample,::downsample,:]
        plt.imshow(small_image[::1,::-1,:].transpose(1,0,2),
                  extent=[pos[i] + s*image.shape[i] 
                          for i in [0,1] for s in [-0.5,0.5]],
                 )
    if outlines:#plot the outlines
        square = np.array([[-1,-1],[-1,1],[1,1],[1,-1],[-1,-1]])
        for pos in positions:
            rect = pos + np.array(image.shape[:2]) * 0.5 * square
            plt.plot(rect[:,0],rect[:,1])
    if centres:#plot the centre of each image
        for i, pos in enumerate(positions):
            plt.text(pos[0],pos[1],i)
    # ax.autoscale()
    plt.savefig('situ.png')


def compareplot(a,b, gain=10, axes=None):
    """Plot two sets of coordinates, highlighting their differences.
    
    a, b: numpy.ndarray
        An Nx2 array of points
    gain: float
        The amount to amplify differences when plotting arrows
    axes: matplotlib.Axes
        An axes object in which we make the plot - otherwise a new one
        will be created.
    """

    # centre = np.mean(a, axis=0)[:2]
    # a = np.array(a) - centre[:2]

    # a = a * 10

    # if axes is None:
    #     f, axes = plt.subplots(1,1)
    # axes.plot(a[:,0],a[:,1], '.')
    # axes.plot(b[:,0],b[:,1], '.')
    # for ai, bi in zip(a,b):
    #     # axes.arrow(ai[0],ai[1],(bi[0]-ai[0])*10,(bi[1]-ai[1])*10)
    #     axes.arrow(ai[0],ai[1],(bi[0]-ai[0])*gain,(bi[1]-ai[1])*gain)


def find_overlapping_pairs(pixel_positions, image_size,
                           fractional_overlap=0.1):
    """Identify pairs of images with significant overlap.
    
    Given the positions (in pixels) of a collection of images (of given size),
    calculate the fractional overlap (i.e. the overlap area divided by the area
    of one image) and return a list of images that have significant overlap.
    
    Arguments:
    pixel_positions: numpy.ndarray
        An Nx2 array, giving the 2D position in pixels of each image.
    image_size: numpy.ndarray
        An array of length 2 giving the size of each image in pixels.
    fractional_overlap: float
        The fractional overlap (overlap area divided by image area) that two
        images must have in order to be considered overlapping.
    
    Returns: [(int,int)]
        A list of tuples, where each tuple describes two images that overlap.
        The second int will be larger than the first.
    """
    tile_pairs = []
    for i in range(pixel_positions.shape[0]):
        for j in range(i+1, pixel_positions.shape[0]):
            overlap = image_size - np.abs(pixel_positions[i,:2] -
                                          pixel_positions[j,:2])
            overlap[overlap<0] = 0
            tile_pairs.append((i, j, np.product(overlap)))
    overlap_threshold = np.prod(image_size)*fractional_overlap
    overlapping_pairs = [(i,j) for i,j,o in tile_pairs if o>overlap_threshold]

    for pair in overlapping_pairs:
        if pair[0] > pair[1]:
            print(pair)

    # print(f'Number of overlapping pairs is {len(overlapping_pairs)}. Number of images is {np.max(np.array(overlapping_pairs))}. Estimated time to find all overlaps is {int(len(overlapping_pairs) * 4 * np.max(image_size) / (3280))} seconds')

    return overlapping_pairs

def high_pass_fourier_mask(shape, s, rfft=True):
    """Generate a mask performing a high pass filter
    
    The return value is a 2D array, which can be multiplied
    with the Fourier Transform of an image to perform a high
    pass filter.
    
    Arguments:
        shape: tuple of 2 integers
            The shape of the output array
        s: float
            The standard deviation of the Gaussian in real
            space, in pixels
    """
    high_pass_filter = np.ones(shape)
    x, y = (np.arange(n, dtype=float) for n in shape)
    # Beyond the halfway point of the array, frequencies are negative
    x[x.shape[0]//2:x.shape[0]] -= x.shape[0]
    if not rfft: # If it's a real fft, the last axis is halved so we can skip this.
        y[y.shape[0]//2:y.shape[0]] -= y.shape[0]
    x /= np.max(np.abs(x)) * 2  # Normalise so highest frequency is 1/2
    y /= np.max(np.abs(y)) * 2  # Normalise so highest frequency is 1/2
    r2 = x[:, np.newaxis]**2 + y[np.newaxis, :]**2
    # now we multiply by 1-FT(Gaussian kernel with sd of s pixels)
    high_pass_filter -= np.exp(-2*np.pi**2*s**2*r2)
    return high_pass_filter

def high_pass_fft_template(initial_fft, sigma=10, calculate_peak=True):
    """Calculate a high-pass-filtered FT template for tracking
    
    This takes a complex conjugate then attenuates low frequencies.
    The resulting array can be used as a template for tracking.
    
    sigma is the standard deviation in pixels of the Gaussian  used
    in the high pass filter.

    calculate_peak computes the value of the brightest pixel we'd expect
    in a correlation image (i.e. the peak if we correlate the image
    passed in with the template we're generating).  This is stored
    in ``template.attrs["peak_correlation_value"]``
    """
    high_pass_filter = high_pass_fourier_mask(initial_fft.shape, sigma)
    template = np.conj(initial_fft) * high_pass_filter
    if calculate_peak:
        expected_peak = np.mean(template * initial_fft)
        maximum_correlation_value = expected_peak
        return template, maximum_correlation_value
    return template, 0

def initial_fft(image, pad=True):
    image, fft_shape = grayscale_and_padding(image, pad)
    return np.fft.rfft2(image, s=fft_shape)  # NB rfft2 is faster, but a different shape!

#################### Cross-correlate overlapping pairs to match them up #######
def crosscorrelate_overlapping_images(tiles, overlapping_pairs, 
                                     pixel_positions, 
                                     fractional_margin=0.01,
                                     pad = True,
                                     resize=0.6,
                                     cache = {},
                                     high_pass_sigma=50):
    """Calculate actual displacements between pairs of overlapping images.
    
    For each pair of overlapping images, perform a cross-correlation to
    fine-tune the displacement between them.
    
    Arguments:
    tiles: [h5py.Dataset]
        A list of datasets (or numpy images) that represent the images.
    overlapping_pairs: [(int,int)]
        A list of tuples, where each tuple describes two images that overlap.
    pixel_positions: numpy.ndarray
        An Nx2 array, giving the 2D position in pixels of each image.
    fractional_margin: float
        Allow for this much error in the specified positions (given as a 
        fraction of the length of the smaller side of the image).  Defaults to
        0.02 which should be fine for our typical microscope set-ups.
    
    Results: np.ndarray
        An Mx2 array, giving the displacement in pixels between each pair of
        images specified in overlapping_pairs.
    """
    pair_data = {}

    os.makedirs('pairs', exist_ok=True)
    files = glob.glob('pairs/*')
    for f in files:
        os.remove(f)

    initial_ffts = {}

    def load_image(i):
        """Load and resize an image"""
        return cv2.resize(cv2.imread(tiles[i], -1) , dsize = [0,0], fx = resize, fy = resize)

    img_0 = load_image(0)
    image_size = img_0.shape[:2]
    initial_fft_0 = initial_fft(img_0, pad=pad)
    high_pass_filter = high_pass_fourier_mask(initial_fft_0.shape, high_pass_sigma)
    del img_0, initial_fft_0

    with alive_bar(len(overlapping_pairs), force_tty=True, dual_line=False) as bar:  # declare your expected total
        for h, (i, j) in enumerate(overlapping_pairs):
            bar.text = f'-> Finding overlap between: {i} and {j}'
            if not cache or f'{i}, {j}' not in cache['pairs'].keys():

                original_displacement = np.round(pixel_positions[j,:2] - pixel_positions[i,:2])
                overlap_size = image_size - np.abs(original_displacement)
                assert np.all(overlap_size <= image_size), "Overlaps can't be bigger than the image"

                # if not i in initial_ffts:
                #     initial_ffts[i] = initial_fft(load_image(i))
                
                # if not j in initial_ffts:
                #     initial_ffts[j] = initial_fft(load_image(j))

                # trial_peak, corr = displacement_from_fft_template(
                #     np.conj(initial_ffts[i]) * high_pass_filter,
                #     initial_ffts[j],
                #     pad=pad,
                #     fractional_threshold=0.02
                # )

                initial_ffts_i = initial_fft(load_image(i))
                initial_ffts_j = initial_fft(load_image(j))

                trial_peak, corr = displacement_from_fft_template(
                    np.conj(initial_ffts_i) * high_pass_filter,
                    initial_ffts_j,
                    pad=pad,
                    fractional_threshold=0.02
                )

                trial_peak *= -1

                pair_data[f'{i}, {j}'] = {}

                for thresh in [0.5, 0.75, 0.9, 0.95]:
                    background = np.min(corr) + thresh * (np.max(corr) - np.min(corr))
                    background_subtracted = corr - background
                    under_thresh = np.count_nonzero(background_subtracted < 0)

                    pair_data[f'{i}, {j}'][f'{thresh} thresh'] = under_thresh / np.count_nonzero(corr)
                
                pair_data[f'{i}, {j}']['peak loc'] = trial_peak

                # if not any(i in pair for pair in overlapping_pairs[h+1:]):
                #     del initial_ffts[i]

                # if not any(j in pair for pair in overlapping_pairs[h+1:]):
                #     del initial_ffts[j]
                
            else:
                pair_data[f'{i}, {j}'] = cache['pairs'][f'{i}, {j}']
                
            bar()  

        # if not any(i in sublist for sublist in overlapping_pairs[h+1:]):
        #     del templates[i]

        # plot_pairs(trial_peak, img_i, img_j, i, j, corr, 'white', 140)

    # print(pair_data)
    return pair_data

def verify_overlaps(pairs, pair_data, pixel_positions, x_thresh, y_thresh):

    correlated_pixel_displacements = []
    truly_overlapping_pairs = []
    true_displacements = []
    transforms = []

    for h, (i, j) in enumerate(pairs):
        
        original_displacement = np.round(pixel_positions[j,:2] - pixel_positions[i,:2])

        data = pair_data['pairs'][f'{i}, {j}']

        # conf = [np.round(under_thresh / auto_1_under_thresh, 4), np.round(under_thresh / auto_2_under_thresh, 4)]
        # print(i, j, auto_1_under_thresh, auto_2_under_thresh, under_thresh, conf)
        # print((np.max(auto_corr_1) - np.max(corr))/ np.max(auto_corr_1), (np.max(auto_corr_2) - np.max(corr))/ np.max(auto_corr_2))

        transform = np.array((np.array(data['peak loc']) - original_displacement))

        thresh_cutoff = x_thresh
        dist_cutoff = y_thresh

        if data['0.9 thresh'] < thresh_cutoff or np.sum(np.abs(transform)) > dist_cutoff or np.sum(np.abs(data['peak loc'])) < 1:
        # if np.sum(transform) > 400*resize:
            colour = 'lightsalmon'
            dpi = 140
        else:
            correlated_pixel_displacements.append(data['peak loc'])
            truly_overlapping_pairs.append(pairs[h])
            true_displacements.append(original_displacement)
            colour = 'white'
            dpi=140

        # plot_pairs(data['peak loc'], img_i, img_j, i, j, corr, colour, dpi)
        transforms.append([[data['0.9 thresh'], np.sum(np.abs(transform))], colour])
        # print(i, j, adjusted_peak, original_displacement, transforms[-1])
        
    # print(transforms[0], transforms[1])
    correlated_pixel_displacements = np.array(correlated_pixel_displacements)
    true_displacements = np.array(true_displacements)
    truly_overlapping_pairs = np.array(truly_overlapping_pairs)

    for transform in transforms:
        # print(transform)
        if transform[1] == 'white':
            color = 'blue'
        else:
            color = 'red'
        plt.scatter(transform[0][0], transform[0][1], color = color)
    # plt.xscale('log')
    plt.axhline(dist_cutoff)
    plt.axvline(thresh_cutoff)
    plt.xlim([thresh_cutoff,1])
    plt.ylim([0,dist_cutoff])
    plt.xlabel('Peak quality')
    plt.ylabel('Stage and correlation difference')
    # plt.show()
    plt.savefig('plot.png')

    # correlated_pixel_displacements, truly_overlapping_pairs = ransac(correlated_pixel_displacements, truly_overlapping_pairs, true_displacements)

    return correlated_pixel_displacements, truly_overlapping_pairs

def ransac_pairs(correlated_pixel_displacements, overlapping_pairs, stage_displacements):
    remove_pairs = []
    remove_displacements = []

    for i in range(0, 2):
        X = stage_displacements[:,i].reshape(-1,1)
        y = correlated_pixel_displacements[:,i]


        # Fit line using all data
        lr = linear_model.LinearRegression()
        lr.fit(X, y)

        # Robustly fit linear model with RANSAC algorithm
        ransac = linear_model.RANSACRegressor()#residual_threshold=500*resize)
        ransac.fit(X, y)
        inlier_mask = ransac.inlier_mask_
        outlier_mask = np.logical_not(inlier_mask)

        # Predict data of estimated models
        line_X = np.arange(X.min(), X.max())[:, np.newaxis]
        line_y = lr.predict(line_X)
        line_y_ransac = ransac.predict(line_X)

        # Compare estimated coefficients
        print("Estimated coefficients (linear regression, RANSAC):")
        print(lr.coef_, ransac.estimator_.coef_)

        lw = 2
        plt.scatter(
            X[inlier_mask], y[inlier_mask], color="black", marker=".", label="Inliers"
        )
        plt.scatter(
            X[outlier_mask], y[outlier_mask], color="red", marker=".", label="Outliers"
        )
        plt.plot(line_X, line_y, color="navy", linewidth=lw, label="Linear regressor")
        plt.plot(
            line_X,
            line_y_ransac,
            color="cornflowerblue",
            linewidth=lw,
            label="RANSAC regressor",
        )

        remove_pairs.append(overlapping_pairs[~inlier_mask,:])
        remove_displacements.append(correlated_pixel_displacements[~inlier_mask,:])
        
        plt.legend()
        plt.xlabel("Stage recorded offset")
        plt.ylabel("Correlation offset")
        # plt.savefig(f'ransac_{i}.png', dpi = 800)
        # plt.show()

    remove_pairs = [item for sublist in remove_pairs for item in sublist]
    remove_displacements = [item for sublist in remove_displacements for item in sublist]

    # print(remove_pairs)

    overlapping_pairs = overlapping_pairs.tolist()
    correlated_pixel_displacements = correlated_pixel_displacements.tolist()

    for pair in remove_pairs:
        try:
            overlapping_pairs.remove(list(pair))
        except:
            pass

    print(remove_pairs)

    for displacement in remove_displacements:
        print(displacement)
        try:
            correlated_pixel_displacements.remove(list(displacement))
        except:
            pass

    overlapping_pairs = np.array(overlapping_pairs)
    correlated_pixel_displacements = np.array(correlated_pixel_displacements)
    return overlapping_pairs, correlated_pixel_displacements

def pull_usercomment_dict(filepath):
    """
    Reads UserComment Exif data from a file, and returns the contained bytes as a dictionary.
    Args:
        filepath: Path to the Exif-containing file
    """
    try:
        exif_dict = piexif.load(filepath)
    except piexif._exceptions.InvalidImageDataError:
        logging.warning("Invalid data at {}. Skipping.".format(filepath))
        return None
    if "Exif" in exif_dict and piexif.ExifIFD.UserComment in exif_dict["Exif"]:
        try:
            m = piexif.load(filepath)

            return pickle.loads(m['Exif'][37510])
        except:
            try:
                return json.loads(exif_dict["Exif"][piexif.ExifIFD.UserComment].decode())
            except json.decoder.JSONDecodeError:
                logging.error(
                    f"Capture {filepath} has old, corrupt, or missing OpenFlexure metadata. Unable to reload to server."
                )
    else:
        return None

def plot_overlaps(pairs, pair_data, pixel_positions):
    transforms = []

    for h, (i, j) in enumerate(pairs):
        data = pair_data['pairs'][f'{i}, {j}']

        original_displacement = np.round(pixel_positions[j,:2] - pixel_positions[i,:2])
        transform = np.array((np.array(data['peak loc']) - original_displacement))
        transforms.append([data['0.9 thresh'], np.sum(np.abs(transform))])

    transforms = np.array(transforms)

    x_data = transforms[:,0]
    y_data = transforms[:,1]

    x_mean = np.mean(x_data)
    x_std = np.std(x_data)

    y_mean = np.mean(y_data)
    y_std = np.std(y_data)

    number_of_deviations = 1

    # for transform in transforms:
    #     plt.scatter(transform[0], transform[1])
    # # plt.xscale('log')
    # plt.axhline(number_of_deviations*y_std)
    # plt.axvline(1-number_of_deviations*x_std)
    # plt.axhline(0, color = 'red')
    # plt.axvline(1, color = 'red')
    
    # plt.xlabel('Peak quality')
    # plt.ylabel('Stage and correlation difference')
    # plt.show()

    # print(f'Best guess for cutoffs is x cutoff: {round(1-number_of_deviations*x_std, 6)}, y cutoff: {round(number_of_deviations*y_std)}')

def pair_displacements(pairs, positions):
    """Calculate the displacement between each pair of positions specified.
    
    Arguments:
    pairs: [(int,int)]
        A list of tuples, where each tuple describes two images that overlap.
    positions: numpy.ndarray
        An Nx2 array, giving the 2D position in pixels of each image.
    
    Result: numpy.ndarray
        An Mx2 array, giving the 2D displacement between each pair of images.
    """
    return np.array([positions[j,:]-positions[i,:] for i,j in pairs])

                                            
def fit_affine_transform(pairs, positions, displacements):
    """Find an affine transform to make positions match a set of displacements.
    
    Find an affine tranform (i.e. 2x2 matrix) that, when applied to the 
    positions, matches the specified pair displacements as closely as possible.
    
    Arguments:
    pairs: [(int,int)]
        A list of M tuples, where each tuple describes two images that overlap.
    positions: numpy.ndarray
        An Nx2 array, giving the 2D position in pixels of each image.
    displacements: numpy.ndarray
        An Mx2 array, giving the 2D displacement between each pair of images.
    
    Result: numpy.ndarray, numpy.ndarray
        A tuple of two things: firstly, a 2x2 matrix that transforms the given
        pixel positions to match the displacements.  Secondly, the positions
        so transformed (i.e. the corrected positions).
    """

    starting_displacements = pair_displacements(pairs, positions)

    affine_transform = np.linalg.lstsq(starting_displacements, 
                                       displacements, rcond=-1)[0]

    corrected_positions = np.dot(positions, affine_transform)
    
    return affine_transform, corrected_positions
    
def apply_affine_to_unknown(pairs, positions, starting_positions, affine_transform):
    """Find an affine transform to make positions match a set of displacements.
    
    Apply the affine transform to images that don't have any overlaps to estimate
    their position. Will only be a rough fit, but will position them in the right
    grid position, just not with ideal overlaps
    
    Arguments:
    pairs: [(int,int)]
        A list of M tuples, where each tuple describes two images that overlap.
    positions: numpy.ndarray
        An Nx2 array, giving the 2D position in pixels of each image.
    starting_positions: numpy.ndarry

    
    Result: numpy.ndarray, numpy.ndarray
        A tuple of two things: firstly, a 2x2 matrix that transforms the given
        pixel positions to match the displacements.  Secondly, the positions
        so transformed (i.e. the corrected positions).
    """
    corrected_positions = []

    for count, position in enumerate(positions):
        if any(count in sl for sl in pairs):
            corrected_positions.append(position)
        else:
            corrected_positions.append(np.dot(starting_positions[count], affine_transform))

    return np.array(corrected_positions)


def rms_error(pairs, positions, displacements, print_err=False):
    """Find the RMS error in image positons (against some given displacements)
    
    Arguments:
    pairs: [(int,int)]
        A list of M tuples, where each tuple describes two images that overlap.
    positions: numpy.ndarray
        An Nx2 array, giving the 2D position in pixels of each image.
    displacements: numpy.ndarray
        An Mx2 array, giving the 2D displacement between each pair of images.
    print_err: bool
        If true, print the RMS error to the console.
    
    Returns: float
        The RMS difference between the displacements calculated from the given
        positions and the displacements specified.
    """
    error = np.std(pair_displacements(pairs, positions) - displacements)
    if print_err:
        print ("RMS Error: %.2f pixels" % error)
    return error
        

###################### Look at each image and shift it to the optimal place ###
def optimise_positions(pairs, positions, displacements):
    """Adjust the positions slightly so they better match the displacements.
    
    After fitting an affine transform (which takes out calibration error) we 
    run this method: it moves each tile to minimise the difference between 
    measured and current displacements.
    
     Arguments:
    pairs: [(int,int)]
        A list of M tuples, where each tuple describes two images that overlap.
    positions: numpy.ndarray
        An Nx2 array, giving the 2D position in pixels of each image.  NB this
        will be modified!  Copy it first if you don't want that.
    displacements: numpy.ndarray
        An Mx2 array, giving the 2D displacement between each pair of images.
    
    Returns: numpy.ndarray
        The modified positions array.
    
    Side Effects:
        "positions" will be modified so it better matches displacements.
    """
    positions_d = pair_displacements(pairs, positions)
    for k in range(positions.shape[0]): #for each particle
        # find the displacement from here to each overlapping image
        measured_d = np.array([d if i==k else -d 
                               for (i,j), d in zip(pairs, displacements)
                               if i==k or j==k])
        current_d = np.array([d if i==k else -d 
                               for (i,j), d in zip(pairs, positions_d)
                               if i==k or j==k])
        if len(measured_d) == 0:
            pass
            # print(f'no overlaps found for tile {k}')
        # shift the current tile in the direction suggested by measured 
        # displacements
        #print "shift:", np.mean(measured_d-current_d,axis=0)
        else:
            positions[k,:] -= np.mean(measured_d-current_d,axis=0) * 0.8
    return positions
        
################### Stitch the images together ################################
def stitch_images(tiles, positions, downsample=3):
    """Merge images together, using supplied positions (in pixels).
    
    Currently we use a crude algorithm - we pick the pixel from whichever image
    is closest.
    
    Arguments:
    tiles: [h5py.Dataset]
        A list of datasets (or numpy images) that represent the images.
    positions: numpy.ndarray
        An Nx2 array, giving the 2D position in pixels of each image.
    downsample: int
        The size of the stitched image will be reduced by this amount in each
        dimension, to save on resources.  NB currently it decimates rather than
        taking a mean, for speed - in the future a mean may be an option.
        Images are downsampled after taking into account their position, i.e.
        if you downsample by a factor of 5, you'll still be within 1 original
        pixel, not within 5, of the right position.  Currently we don't do
        any sub-pixel shifting.
        
    Returns: (stitched_image, stitched_centre, image_centres)
        (numpy.ndarray, numpy.ndarray, numpy.ndarray)
        An MxPx3 array containing the stitched image, a 1D array of length
        2 containing the coordinates of the centre of the image in non-
        downsampled pixel coordinates, and an Nx2 array of the positions of the
        source images (their centres) relative to the top left of the stitched
        image, in downsampled pixels.
    """
    positions = positions.copy() #prevent unpleasant side effects
    use_tiles = deepcopy(tiles)
    np.savez('positions.npz', positions)
    # first, work out the size and position of the stitched image

    first_img = cv2.imread(tiles[0], -1)
    image_size = np.array(first_img.shape[:2])

    stitched_size = np.round((np.max(positions, axis=0) - np.min(positions, axis=0) + image_size)/downsample)
    stitched_centre = (np.max(positions, axis=0) + np.min(positions, axis=0))/2
    stitched_image = np.zeros(tuple(stitched_size.astype(int))+(3,), dtype=np.uint8)
    # now calculate the position of each image relative to the big image
    stitched_centres = (positions - stitched_centre 
                        + np.array(stitched_image.shape[:2])*downsample/2.0)/downsample
    
    for tile_link, centre in zip(use_tiles, stitched_centres):
        topleft = centre - image_size/2.0/downsample
        #topleft is the (downsampled) pixel coordinates of the tile's corner in the big image
        shift = (np.ceil(topleft) - topleft) * downsample

        # tile = cv2.resize(cv2.imread(tile_link, -1), dsize=[0,0], fx=1/downsample, fy=1/downsample)
        tile = cv2.imread(tile_link, -1)
        w, h, d = tile.shape
        img = np.zeros((w//downsample - 1, h//downsample - 1, d), dtype=int)
        # Crudely downsample (TODO: take a mean rather than downsampling)
        # for dx in range(downsample):
        #    for dy in range(downsample):
        #        img += tile[shift+dx:shift+dx+downsample*img.shape[0]:downsample,
        #                    shift+dy:shift+dy+downsample*img.shape[1]:downsample,
        #                    :] / downsample**2
        img = tile[int(shift[0]):int(shift[0]+downsample*img.shape[0]):downsample,
                   int(shift[1]):int(shift[1]+downsample*img.shape[1]):downsample,
                   :]
        stitched_image[int(np.ceil(topleft[0])):int(np.ceil(topleft[0])+img.shape[0]),
                       int(np.ceil(topleft[1])):int(np.ceil(topleft[1])+img.shape[1]),:] = img 
    return stitched_image, stitched_centre, stitched_centres

################### Stitch the images together ################################
def stitch_centre_of_images(tiles, positions, downsample=3):
    """Merge images together, using supplied positions (in pixels).
    
    Currently we use a crude algorithm - we pick the pixel from whichever image
    is closest.
    
    Arguments:
    tiles: [h5py.Dataset]
        A list of datasets (or numpy images) that represent the images.
    positions: numpy.ndarray
        An Nx2 array, giving the 2D position in pixels of each image.
    downsample: int
        The size of the stitched image will be reduced by this amount in each
        dimension, to save on resources.  NB currently it decimates rather than
        taking a mean, for speed - in the future a mean may be an option.
        Images are downsampled after taking into account their position, i.e.
        if you downsample by a factor of 5, you'll still be within 1 original
        pixel, not within 5, of the right position.  Currently we don't do
        any sub-pixel shifting.
        
    Returns: (stitched_image, stitched_centre, image_centres)
        (numpy.ndarray, numpy.ndarray, numpy.ndarray)
        An MxPx3 array containing the stitched image, a 1D array of length
        2 containing the coordinates of the centre of the image in non-
        downsampled pixel coordinates, and an Nx2 array of the positions of the
        source images (their centres) relative to the top left of the stitched
        image, in downsampled pixels.
    """
    positions = positions.copy() #prevent unpleasant side effects
    use_tiles = deepcopy(tiles)
    # first, work out the size and position of the stitched image

    first_img = cv2.imread(tiles[0], -1)

    image_size = np.array(first_img.shape[:2])

    stitched_size = np.round((np.max(positions, axis=0) - np.min(positions, axis=0) + image_size)/downsample)
    stitched_centre = (np.max(positions, axis=0) + np.min(positions, axis=0))/2
    stitched_image = np.zeros(tuple(stitched_size.astype(int))+(3,), dtype=np.uint8)
    # now calculate the position of each image relative to the big image
    stitched_centres = (positions - stitched_centre 
                        + np.array(stitched_image.shape[:2])*downsample/2.0)/downsample
    
    for tile_link, centre in zip(use_tiles, stitched_centres):
        topleft = centre - image_size/2.0/downsample
        #topleft is the (downsampled) pixel coordinates of the tile's corner in the big image
        shift = (np.ceil(topleft) - topleft) * downsample

        tile = cv2.imread(tile_link, -1)
        w, h, d = tile.shape
        img = np.zeros((w//downsample - 1, h//downsample - 1, d), dtype=int)
        # Crudely downsample (TODO: take a mean rather than downsampling)
        # for dx in range(downsample):
        #    for dy in range(downsample):
        #        img += tile[shift+dx:shift+dx+downsample*img.shape[0]:downsample,
        #                    shift+dy:shift+dy+downsample*img.shape[1]:downsample,
        #                    :] / downsample**2
        img = tile[int(shift[0]):int(shift[0]+downsample*img.shape[0]):downsample,
                   int(shift[1]):int(shift[1]+downsample*img.shape[1]):downsample,
                   :]
        # Now, we zero out the tile for all the pixels where the centre of
        # another image is closer than the centre of this one
        for other_centre in stitched_centres:
            if np.any(other_centre != centre): # don't compare to this image
                difference = other_centre - centre
                midpoint = (other_centre + centre)/2
                for yi in range(img.shape[1]):
                    y = np.ceil(topleft[1]) + yi
                    xi_threshold = np.ceil(midpoint[0] 
                                           - (y - midpoint[1])*difference[1]/difference[0] 
                                           - np.ceil(topleft[0]))
                    # we pretty much want to set pixels to zero where
                    # difference.(x - midpoint) > 0
                    # FIXME: this might go wrong if ever we hit zero exactly...
                    if difference[0] > 0:
                        if xi_threshold < 0:
                            xi_threshold = 0
                        if xi_threshold < img.shape[0]:
                            img[int(xi_threshold):,int(yi),:] = 0
                    else:
                        if xi_threshold > img.shape[0]:
                            xi_threshold = img.shape[0]
                        if xi_threshold > 0:
                            img[:int(xi_threshold),int(yi),:] = 0
        #print "Inserting image at {0}, size {1}, canvas size {2}".format(
        #        np.ceil(topleft), img.shape, stitched_image.shape)
        stitched_image[int(np.ceil(topleft[0])):int(np.ceil(topleft[0])+img.shape[0]),
                       int(np.ceil(topleft[1])):int(np.ceil(topleft[1])+img.shape[1]),:] += img 
    return stitched_image, stitched_centre, stitched_centres

def plot_pairs(peak, tiles, i, j, colour, dpi):

    img_i = cv2.imread(tiles[i], -1)
    # img_j = cv2.imread(tiles[j], -1)

    # fig, axs = plt.subplots(2,2)
    # axs[0, 0].imshow(img_i)
    # axs[0, 0].scatter(peak[1], peak[0], edgecolors = 'red', facecolors = 'none')
    # axs[0, 1].imshow(img_j)
    # # axs[1, 0].imshow(corr)
    # # axs[1, 0].scatter(peak[1], peak[0], edgecolors = 'red', facecolors = 'none')

    # # axs[1, 0].set_xlabel(under_thresh / np.count_nonzero(corr))
    # peak = np.asarray(peak).astype(int)

    # canvas = np.zeros([int(abs(peak[0]) + img_i.shape[0]), int(abs(peak[1]) + img_i.shape[1]), 3])
    # if peak[1] >= 0:
    #     if peak[0] >= 0 :
    #         canvas[:img_i.shape[0], :img_i.shape[1]] = img_i
    #         canvas[peak[0]:peak[0]+img_i.shape[0], peak[1]:peak[1]+img_i.shape[1]] = img_j
    #     else:
    #         canvas[-peak[0]:-peak[0]+img_i.shape[0], :img_i.shape[1]] = img_i
    #         canvas[:img_i.shape[0],peak[1]:peak[1]+img_i.shape[1]] = img_j
    # else:
    #     if peak[0] >= 0 :
    #         canvas[:img_i.shape[0],-peak[1]:-peak[1]+img_i.shape[1]] = img_i
    #         canvas[peak[0]:peak[0]+img_i.shape[0], :img_i.shape[1]] = img_j
    #     else:
    #         canvas[-peak[0]:-peak[0]+img_i.shape[0],-peak[1]:-peak[1]+img_i.shape[1]] = img_i
    #         canvas[:img_i.shape[0], :img_i.shape[1]] = img_j
    # axs[1,1].imshow(canvas.astype(np.uint8))
    # # plt.show()
    # fig.savefig('pairs\{}_{}.png'.format(i, j), dpi = dpi, facecolor = colour)
    # plt.close(fig)

    # plt.imshow(corr)
    # print(under_thresh / np.count_nonzero(corr))
    # plt.savefig(f"{i}_{j}_corr.png")

class PathException(Exception):
    pass

def reconstruct_tiled_image(dir,
                            img_list,
                            camera_to_sample=np.identity(2),
                            downsample=1,
                            resize=0.6,
                            unknown_images = "warn",
                            fractional_overlap = 0.3,
                            img_limit = 0):
    """Combine a sequence of images into a large tiled image.
    
    This function takes a list of images and approximate positions.  It first
    aligns the images roughly, then uses crosscorrelation to find relative
    positions of the images.  Positions of the images are optimised and the
    tiles are then stitched together with a crude pick-the-closest-image-centre
    algorithm.  Importantly, we keep careful track of the relationship between
    pixels in the original images, their positions (in "sample" units), and the
    same features in the stitched image.
    
    Arguments:
    folder: str
        The folder containing images to be tiled. Each image should have
        OpenFlexure metadata containing its stage position and the 
        Camera Stage Mapping matrix to estimate its pixel position.
    camera_to_sample: numpy.ndarray
        The camera_to_sample matrix maps displacement in camera units (where 
        the width or height of an image is 1) into sample units (generally
        corresponding to actual distance).
    downsample: int
        Downsampling factor (produces a less huge output image).  Only applies
        to the final stitching step.
    resize: float
        Resizing factor for the calculations and positioning, but not the
        final image. Higher is slower but likely more accurate
    unknown_images: str
        How to treat tiles with images that don't appear to overlap.
        If "break", return an error message
        If "ignore", attempt to position this images roughly in line with
        the estimated affine transform
        If "warn", behave like "ignore", but print a warning to the console
    
    Returns: dict
        This function returns a dictionary with various elements:
        stitched_image: numpy.ndarray
            The stitched image as a numpy array
        stitched_to_sample: numpy.ndarray
            A mapping matrix that transforms from image coordinates (0-1 for X 
            and Y) to "sample" coordinates (whatever the original positions 
            were specified in)
        stitched_centre: numpy.ndarray
            The centre of the stitched image, again in sample coordinates.
        image_centres: numpy.ndarray
            The centres of the tiles, in pixels in the stitched image (mostly 
            for debug purposes, but can be useful to locate the original tile 
            for a given pixel).  It has dimensions Nx2, where N is the number
            of source images.
        corrected_camera_to_sample: numpy.ndarray
            The 2x2 mapping matrix passed as input, tweaked to better match
            the measured displacements between the images, and their given
            positions.
    """
    tiles, pixel_positions, scan_centre, stage_positions = get_pixel_positions(dir, img_list, camera_to_sample, resize, img_limit)

    starting_pixel_positions = pixel_positions.copy()
    
    first_img = cv2.resize(cv2.imread(tiles[0], -1), dsize = [0,0], fx = resize, fy = resize)
    image_size = np.array(first_img.shape[:2])
    
    # then find images estimated to have at least fractional_overlap in common
    pairs = find_overlapping_pairs(pixel_positions, image_size, fractional_overlap)
    string_pairs = []

    for i in range(len(pairs)):
        string_pairs.append(f'{pairs[i][0]}, {pairs[i][1]}')
    try:
        with open(os.path.join(dir, 'export.pkl'), 'rb') as f:
            imported = pickle.load(f)

        cached_pair_data = imported
        
        if string_pairs != list(imported['pairs'].keys()):
            print('Inputs have changed')
            raise Exception

        if imported['resize'] != resize:
            print('Inputs have changed')
            raise Exception

        if not np.array_equal(imported['CSM'], camera_to_sample):
            print('Inputs have changed')
            raise Exception
        
        pair_data = cached_pair_data
    
        print('Cache successful, skipping to thresholding')

    except:
        print('Cache failed, redoing overlaps (may take some time)')

        all_sites = sites_linked(pairs, len(tiles))

        if all_sites is False:
            if unknown_images == "break":
                raise PathException("Not all sites overlap enough according to your inputs, check your images and update the CSM or overlap")
            elif unknown_images == "warn":
                print("Not all sites overlap enough according to your inputs, continuing")

        pair_data = {}
        try:
            with open(os.path.join(dir, 'export.pkl'), 'rb') as f:
                cached_pair_data = pickle.load(f)
        except:
            print('There is no cache to import')            
            cached_pair_data = {}

        # compare overlapping images to find the true displacement between them
        pair_data['pairs'] = crosscorrelate_overlapping_images(tiles, pairs, pixel_positions, 
                                                        fractional_margin=0.04, pad = True, resize = resize, cache = cached_pair_data)
        
        pair_data['resize'] = resize
        pair_data['CSM'] = camera_to_sample

        with open(os.path.join(dir, 'export.pkl'), 'wb') as f:
            pickle.dump(pair_data, f)

    plot_overlaps(pairs, pair_data, pixel_positions)

    return pairs, pair_data, pixel_positions, tiles, unknown_images, camera_to_sample, resize, fractional_overlap, starting_pixel_positions, scan_centre

def stitch_from_thresholds(dir, pairs, pair_data, pixel_positions, tiles, unknown_images, camera_to_sample, resize, fractional_overlap, starting_pixel_positions, scan_centre, x_thresh, y_thresh, img_limit, downsample):
    
    displacements, pairs = verify_overlaps(pairs, pair_data, pixel_positions, x_thresh, y_thresh)

    all_sites = sites_linked(pairs, len(tiles))
    if all_sites is False:
        if unknown_images == "break":
            raise PathException("Not all images overlap, try lowering cross correlate theshold")
        elif unknown_images == "warn":
            print("Not all sites overlap enough according to your inputs, continuing")

    # now we start the optimisation...
    errors = [rms_error(pairs, pixel_positions, displacements, print_err=True)]


    # first, fit an affine transform, to correct for the calibration between
    # camera and stage being slightly off (rotation, scaling, etc.)
    affine_transform, pixel_positions = fit_affine_transform(pairs, pixel_positions,
                                                       displacements)

    errors.append(rms_error(pairs, pixel_positions, displacements, print_err=False))

    print("Optimising image positions...")
    # next, optimise the positions iteratively
    while (errors[-2] - errors[-1]) > 0.001: #len(errors) < 5 or
        pixel_positions = optimise_positions(pairs, pixel_positions, displacements)
        errors.append(rms_error(pairs, pixel_positions, displacements, print_err=False))
        if len(errors) > 500:
        # if len(errors) > 100:
            break

    # plt.plot(errors,'.')
    # plt.show()

    rms_error(pairs, pixel_positions, displacements, print_err=True)

    tweak_affine_transform, _ = fit_affine_transform(pairs, pixel_positions,
                                                       displacements)

    improved_affine_transform = np.dot(affine_transform, tweak_affine_transform)

    try:
        corrected_camera_to_sample = np.dot(np.linalg.inv(affine_transform),
                                            camera_to_sample)
        improved_corrected_camera_to_sample = np.dot(tweak_affine_transform,
                                            corrected_camera_to_sample)
        # print(f"Input is {camera_to_sample}")                                    
        # print(f"First loop is {corrected_camera_to_sample}")
        # print(f"Final loop is {np.array2string(improved_corrected_camera_to_sample, separator=',')}")
    except:
        improved_corrected_camera_to_sample = 0
        print('Singular matrix, can\'t invert')

    # print(f'pairs are {pairs}')

    pixel_positions = apply_affine_to_unknown(pairs, pixel_positions, starting_pixel_positions, improved_affine_transform)

    np.savez(os.path.join(dir, 'scan_settings'), positions=pixel_positions, camera_to_stage=camera_to_sample, resize=resize, unknown_images=unknown_images, fractional_overlap= fractional_overlap, img_limit=img_limit, downsample = downsample)

    return tiles, pixel_positions, improved_corrected_camera_to_sample, scan_centre

def generate_stitched_image(tiles, positions, resize, downsample, csm, scan_centre):
    
    first_img = cv2.resize(cv2.imread(tiles[0], -1), dsize = [0,0], fx = resize, fy = resize)
    image_size = np.array(first_img.shape[:2])

    print("Combining images...")
    
    # plot_images_in_situ(tiles, positions/resize, None, 9, True, True)
    
    stitched_image, stitched_centre, image_centres = stitch_images(tiles, 
                                            positions/resize, downsample=downsample)

    # now work out how the new image relates to the stage coordinates
    # the correct camera-to-sample matrix for source images is 
    # corrected_camera_to_sample; we need to adjust for image size though.
    
    stitched_size = np.array(stitched_image.shape[:2])
    size_in_images =  stitched_size.astype(float) * downsample / image_size
    stitched_to_sample = csm * size_in_images[:, np.newaxis]
    
    shift_in_sample_coords = np.dot(stitched_centre/image_size,
                                    csm)
    stitched_centre = np.append(shift_in_sample_coords,0)[:2] + scan_centre[:2]

    return {'stitched_image': stitched_image, 
            'stitched_to_sample': stitched_to_sample, 
            'stitched_centre': stitched_centre, 
            'image_centres': image_centres,
            'corrected_camera_to_sample': csm}

def background_subtracted_centre_of_mass(corr, fractional_threshold=0.05, quadrant_swap=False):
    """Carry out a background subtracted centre of mass measurement
    
    Arguments:
        corr: a 2D numpy array, to be thresholded
        fractional_threshold: the fraction of the range (from 
            min(corr) to max(corr)) that should remain above 
            the background level.  1 means no thresholding, 
            0.05 means use only the top 5% of the range.
        quadrant_swap: boolean, default False
            Set this to true if we are working on the output of
            a Fourier transform.  This will adjust the coordinates
            such that we effectively perform quadrant swapping, to
            place the DC component in the centre of the image, and
            make the coordinate (0,0) correspond to that point, with
            positive and negative coordinates either side.
    """
    
    assert corr.dtype == float, "The image must be floating point"

    background = np.max(corr) - fractional_threshold * (np.max(corr) - np.min(corr))
    background_subtracted = corr - background
    background_subtracted[background_subtracted < 0] = 0

    xs, ys = (np.arange(n) for n in background_subtracted.shape) # This is equivalent to meshgrid, more or less...
    if quadrant_swap:
        xs[len(xs)//2:] -= len(xs)
        ys[len(ys)//2:] -= len(ys)
    x = np.sum(background_subtracted * xs[:, np.newaxis])
    y = np.sum(background_subtracted * ys[np.newaxis, :])
    I = np.sum(background_subtracted)

    return np.array([x/I, y/I])

def autocorrelate(template, image, pad):
    image, fft_shape = grayscale_and_padding(image, pad)
    corr = np.fft.irfft2(template * np.fft.rfft2(image, s=fft_shape))
    return corr

def displacement_from_fft_template(template, fft_image, fractional_threshold=0.1, pad=True, return_peak=False, error_threshold=0):
    """Find the displacement, in pixels, of an image from a template
    
    The template should be generated by ``high_pass_fft_template``
    Fractional_threshold is the fraction of the range (from max to min)
    of the cross-correlation image that should remain above the threshold
    before finding the peak by centre-of-mass.
    
    NB because of the periodic boundary conditions of the FFT, this gives
    a result that is ambiguous - it's only accurate modulo one image.  
    The result that is returned represents the smalles displacement,
    positive or negative.  You may add or subtract one whole image-width
    (or height) if that makes sense - use other cues to resolve the
    ambiguity.

    return_peak returns the brightest pixel in the correlation image, as
    well as the displacement in a tuple.

    error_threshold is an optional floating-point number between 0 and 1.
    Setting it to a value greater than 0 will compare the correlation value
    with the maximum possible.  If the ratio of the current signal to the
    maximum drops below ``error_threshold``, we raise a ``TrackingError``
    exception.
    """
    # The template is already Fourier transformed and high pass filtered.
    # so multiplying the two in Fourier space performs the convolution.
    corr = np.fft.irfft2(template * fft_image)
    displacement = background_subtracted_centre_of_mass(corr, fractional_threshold, quadrant_swap=True)
    if return_peak:
        return displacement, corr, np.max(corr)
    return displacement, corr

def sites_linked(pairs, sites):

    if len(pairs) == 0:
        return False

    sites_in_chain = [pairs[0][0]]

    cont = True

    while cont == True:
        new_sites_in_chain = sites_in_chain
        for site in new_sites_in_chain:
            for pair in pairs:
                if site in pair:
                    if pair[0] not in new_sites_in_chain:
                        new_sites_in_chain.append(pair[0])
                    elif pair[1] not in new_sites_in_chain:
                        new_sites_in_chain.append(pair[1])
        if sites_in_chain == new_sites_in_chain:
            cont = False

    pair_elements = set(range(sites))
    chain_elements = set(new_sites_in_chain)

    if pair_elements != chain_elements:
        differences = []
        for site in pair_elements:
            if site not in chain_elements:
                differences.append(site)
        print(f'The images not in the conjoined path are {differences}')

    return pair_elements == chain_elements

def grayscale_and_padding(image, pad=True):
    """Convert to grayscale and prepare for zero padding if needed.
    
    The FFT-based tracking methods need grayscale images.  Also, if
    we are going to zero-pad, we should convert to floating point and
    ensure the mean of the image is zero, otherwise the dominant feature
    will be the edge of the image.
    
    Returns:
        image, fft_shape
    """
    if len(image.shape) == 3:
        image = np.mean(image, axis=2)
    fft_shape = np.array(image.shape)
    if pad:
        image = image.astype(float) - np.mean(image)
        fft_shape *= 2
    return image, fft_shape

# if __name__ == "__main__":
    
#     # dir = r'C:\Users\jakna\Source\tiling\97688031-549e-4587-b86c-bec12f4fd2e8\SCAN_2021-12-09_15-20-48'
#     dir = r'C:\Users\jakna\Source\tiling\97688031-549e-4587-b86c-bec12f4fd2e8\SCAN_2021-12-09_15-20-48\small'


#     reconstruction, tiles, positions = reconstruct_tiled_image(dir,
#                                              camera_to_sample=camera_to_sample,
#                                              positioning_error=0.02,
#                                              downsample=10)
#     stitched_image = reconstruction['stitched_image']
#     stitched_to_sample = reconstruction['stitched_to_sample']
#     stitched_centre = reconstruction['stitched_centre']
#     image_centres = reconstruction['image_centres']
    
#     stitched_size = np.array(stitched_image.shape[:2])
#     retransformed_coords = np.dot(image_centres/stitched_size-0.5, 
#                                   stitched_to_sample) + stitched_centre[:2]    
#     actual_coords = np.array([positions[i]
#                               for i, tile in enumerate(tiles)])
#     # check the original positions match (+/- stage error):
#     #compareplot(retransformed_coords, actual_coords)
#     cv2.imwrite("2_dir_stitched_image.jpg", stitched_image[:,:,::-1])
    
#     if False:
#         # A crude interactive example that shows the source tile when the
#         # stitched image is clicked.  Needs a lot of work!
#         corrected_camera_to_sample = reconstruction['corrected_camera_to_sample']
#         image_centres = reconstruction['image_centres']
#         image_size = np.array(tiles[0].shape[:2])
#         stitched_size = np.array(stitched_image.shape[:2])
#         """Make an interactive, clickable plot"""
    
#         f, ax = plt.subplots(1,2)
#         ax[0].imshow(stitched_image)
#         ax[0].plot(image_centres[:,1],image_centres[:,0],'bo')
#         current_tile_index = -1
#         def clicked_stitched_image(event):
#             """Pop up the original image for a clicked point, and show the point"""
#             global current_tile_index
#             here = np.array([event.ydata,event.xdata])
#             tile_index = np.argmin(np.sum((image_centres-here)**2, axis=1))
#             tile = tiles[tile_index]
#             if tile_index != current_tile_index:
#                 current_tile_index = tile_index
#                 ax[1].imshow(tile)
#                 print("now closest to tile %d" % tile_index)
#                 ax[1].autoscale()
            
#             # work out where we clicked in sample coords
#             image_coords = here.astype(float)/np.array(stitched_image.shape[:2])
#             image_coords -= np.array([0.5,0.5])
#             sample_coords = np.dot(image_coords, stitched_to_sample)
#             sample_coords += stitched_centre[:2]
#             print("clicked on sample coords: {0}".format(tuple(sample_coords)))
            
#             # highlight that point in the source image
#             displacement_in_tile = sample_coords - tile.attrs.get('camera_centre_position')[:2]
#             displacement_pixels = image_size * np.dot(displacement_in_tile,
#                                                       np.linalg.inv(corrected_camera_to_sample)) #NO LONGER RETURNED!!
#             pixel_position = displacement_pixels + image_size/2
#             if len(ax[1].lines) == 0:
#                 ax[1].plot(pixel_position[1],pixel_position[0],'r+')
#             else:
#                 ax[1].lines[0].set_data((pixel_position[1],pixel_position[0]))
#             ax[0].figure.canvas.draw()
#             print("pixel position in tile: ", pixel_position)
#         f.canvas.mpl_connect('button_release_event', clicked_stitched_image)

#     """
#     if plot_as_we_go:
#         f, ax = plt.subplots(1,1)
#         ax.set_aspect('auto')
#         ax.plot(errors)
#         ax.set_xlabel("Iteration")
#         ax.set_ylabel("RMS error in image positions")
        
#     if plot_as_we_go:
#         downsample=3
#         f = plt.figure()
#         ax0 = f.add_subplot(1,2,1)
#         ax1 = f.add_subplot(1,2,2,sharex=ax0,sharey=ax0)
#         for ax in [ax0,ax1]:
#             ax.set_aspect(1)
#         #plot the images in position
#     """
