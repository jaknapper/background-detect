# Background detect



## Getting started

This is test code to run a large area scan on the OpenFlexure Microscope, where the irregular shape of the sample necessitates images be taken of the background. If the background is sufficiently sparse, running autofocuses on it as part of a scan can move the optics away from the focal plane, to the point that eventually refocusing on the sample is impossible. This background detection attempts to identify whether the sample is present in a field of view, and will only autofocus if so.

This code should only require an OpenFlexure Microscope, a local installation of Python (3.4 or later) with standard imports and a slide with an obvious background / sample contrast.

Instructions for other python versions (such as Anaconda) and other operating systems are coming soon, and the long term plan is to release this code as an extension in the OpenFlexure software, meaning it can be ran directly from OpenFlexure Connect.

## Setting up the repo

If you use git, clone this repository by navigating in your terminal to your required containing folder. Run 
```
git clone https://gitlab.com/jaknapper/background-detect 
```
to download the files locally. If you're unfamiliar with git, download the files as a zip and extract them.

Create and activate a virtual environment (assuming you're using Windows) from within the downloaded folder from the terminal using 
```
cd background-detect
python -m venv .venv
.\.venv\Scripts\activate
python -m ipykernel install --user --name background_detect
```
Install the necessary libraries using 
```
pip install -r requirements.txt
```

## Using this method

The [ofm_gaussian.ipynb](ofm_gaussian.ipynb) is the notebook required to run a scan. It can be opened in VSCode, or in your browser by running
```
jupyter notebook
```
in your terminal, then opening the notebook from your browser. Make sure that the kernel used is the one named earlier (background detect), so all the required modules are accessible.

Your microscope IP address will be needed if known, otherwise it can be automatically found. The code requires an image of the background of the sample, which should be as representative of the background as possible. A background image from one slide can be used for another similar slide, assuming lighting, set up and slides are kept constant. Different background image options are shown below.

<figure>
<img src="README_examples/022.jpg" style="max-width:32%;" />
<img src="README_examples/013.jpg" style="max-width:32%;" />
<img src="README_examples/192.jpg" style="max-width:32%;" />
<figcaption>First. Unacceptable, includes part of the sample. <br> Second. Usable, but doesn't cover full range of background colours. <br> Third. Best choice, includes dark spots which also make up the background. </figcaption>
</figure>

The background image can either be captured in the notebook by moving to given x, y, z coordinates, or loaded from a path. The image is then converted into LUV colourspace, and the mean and standard deviation of each channel is found.

The user can then set up a raster, snake or spiral scan using the usual parameters. Care should be taken to ensure that the first image of the sample is in focus - this is the sample height that will be assumed correct for the rest of the scan.

The notebook will move to each scan location, and use the distribution of pixel colours to identify the area as background or sample. If background, an image is saved at the current height.

If sample, an autofocus is performed, and the focused height tested against a list of previous focused heights. If this focused height is too far from the previous, the autofocus is rejected and the optics returned to the nearest previous focused height. An image is then captured and saved, before the microscope moves to the next *xy* location.

## Outputs

The images from the scan will be saved in a new folder in [/scans](/scans), numbered in order of capture to make tiling in software such as [ImageJ](https://imagej.nih.gov/ij/) possible. Each scan site will also have a PNG saved showing the image taken, decision on whether the image is sample or background, a masked image showing the pixels considered to be sample, and a 3D scatter plot showing the pixel values of the background and current image. These plots can be useful for debugging, to suggest whether the code is successfully identifying the background, or if thresholds need to be adjusted.

<figure>
<img src="README_examples/00.png"/>
<img src="README_examples/09.png"/>