import numpy as np
from matplotlib import pyplot as plt
import cv2
from scipy.stats import norm
from matplotlib.widgets import Slider
import scatter

# Import the two images to compare
background = cv2.imread(r"C:\Users\jakna\Source\tiling\background_detect\calibrate\20\00.jpg", -1)
img = cv2.imread(r"C:\Users\jakna\Source\tiling\background_detect\calibrate\20\20.jpg", -1)

# Convert the background to LUV and make a list of pixel values from each channel
background_LUV = cv2.cvtColor(background, cv2.COLOR_BGR2LUV)
ch1 = (background_LUV.T[0]).flatten()
ch2 = (background_LUV.T[1]).flatten()
ch3 = (background_LUV.T[2]).flatten()

points = np.array([np.asarray(ch1),np.asarray(ch2),np.asarray(ch3)]).T

# Make an array of the mean and standard deviations for each channel 
mu, std = np.apply_along_axis(norm.fit, 0, points)
stats_list = np.vstack([mu, std])

# Resize the images to speed up processing
img = cv2.resize(img, (0,0), fx=0.4, fy=0.4)

# Initialise lists of the images and amount of sample coverage
imgs = []
coverages = []

# For x between 0 and 14, plot the image with a mask filtering out pixels within x standard deviations of the mean of the background image
for i in range(0, 15):
    image = img.copy()
    image = cv2.cvtColor(image, cv2.COLOR_BGR2LUV)
    img_mask = scatter.check_dist(image, stats_list, i)
    coverages.append(np.count_nonzero(~img_mask)/img_mask.size)
    image[img_mask] = (0,0,0)
    imgs.append(cv2.cvtColor(image, cv2.COLOR_LUV2RGB))

# Set up a plot of the original and masked image, plus an annotation of the coverage percent
fig, ax = plt.subplots(1, 2)
ax[0].imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
plt.subplots_adjust(bottom=0.25)
ax[1].imshow(imgs[0])
ax[1].xaxis.set_label_position('top') 
ax[1].set_xlabel('{0}% sample coverage'.format(round(100*coverages[0],1)))

# Update plot based on the slider value
def update_wave(val):
    idx = int(sliderwave.val)
    ax[1].cla()
    ax[1].imshow(imgs[idx])
    ax[1].set_xlabel('{0}% sample coverage'.format(round(100*coverages[idx],1)))
    fig.canvas.draw_idle()

# Sliders
axwave = plt.axes([0.4, 0.3, 0.5, 0.03])
sliderwave = Slider(axwave, 'Std. multiples from the mean', 0, len(imgs)-1, valinit=0, valfmt='%d')
sliderwave.on_changed(update_wave)

plt.show()