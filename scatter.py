import numpy as np
from matplotlib import pyplot as plt
from scipy.stats import norm
import cv2

import sys
sys.path.insert(0, r'C:\Users\Administrator\Source\openflexure-microscope-pyclient')
import openflexure_microscope_client

# tests whether each pixel in image is within multiple stdevs (stats_list[1]) of the mean (stats_list[0]) for all three channels
# returns a mask with True for pixels within that range (similar to stats list) and False otherwise

def check_dist(image, stats_list, multiple):
    return(
        np.all(np.abs(image - stats_list[np.newaxis, np.newaxis, 0, :]) < stats_list[np.newaxis, np.newaxis, 1, :] * multiple, axis = 2)
    )

# Finds the index of the closest x-y position in a list from the current position, with ties split by the later element in the list (most recently taken)

def closest(current, focused_path):
    
    current_pos = np.array(current[:2], dtype='float64')
    path_pos = np.asarray(focused_path, dtype='float64').T[:2].T

    dist_2 = np.sqrt(np.sum((path_pos - current_pos)**2, axis=1, dtype='float64'), dtype = 'float64')
    min_dist = np.argmin(dist_2)
    mask = np.where(dist_2 == dist_2[min_dist], 1, 0)
    try:
        closest = np.max(np.nonzero(mask))
    except:
        closest = 0
    return(closest)

def unpack_autofocus(scan_data):
    """Extract z, sharpness data from a move_and_measure call
    
    Data will start at `start_index`, i.e. `start_index` points are dropped
    from the beginning of the array.
    """
    jpeg_times = scan_data['jpeg_times']
    jpeg_sizes = scan_data['jpeg_sizes']
    jpeg_sizes_MB = [x / 10**3 for x in jpeg_sizes]
    stage_times = scan_data['stage_times']
    stage_positions = scan_data['stage_positions']
    stage_height = [pos[2] for pos in stage_positions]

    jpeg_heights = np.interp(jpeg_times,stage_times,stage_height)

    def turningpoints(lst):
        dx = np.diff(lst)
        return dx[1:] * dx[:-1] < 0

    turning = np.where(turningpoints(jpeg_heights))[0]+1

    return jpeg_heights[turning[0]:turning[1]], jpeg_sizes_MB[turning[0]:turning[1]]


def looping_autofocus(microscope, dz=2000):
    repeat = True
    attempts = 0
    while repeat:
        data = microscope.autofocus(dz = dz)
        heights, sizes = unpack_autofocus(data)
        if (microscope.position['z'] - min(heights)) < dz / 5 or (max(heights) - microscope.position['z']) < dz / 5:
            print('repeat')
            dz = 4000
            attempts += 1
            if attempts >= 3:
                repeat = False
            pass
        else:
            repeat = False

def limit_focus_change(prev_pos, prev_z, new_pos, new_z, limit):
    # limit is the largest ratio of change in z to change in xy that's allowed

    prev_xy = np.asarray(prev_pos, dtype = 'float64')
    new_xy = np.asarray(new_pos, dtype = 'float64')

    dist = np.sqrt(np.sum(((new_xy - prev_xy)/10**4)**2, dtype = 'float64'))

    focus_change = abs(new_z - prev_z)/10**4
    if dist == 0:
        print(f'Not moved between {prev_pos} and {new_pos}')
        movement_ratio = 0
    else:
        movement_ratio = np.divide(focus_change, dist, dtype='float64')

    # print('Movement ratio is {0} in z per lateral step. The limit is {1}'.format(round(movement_ratio, 4), round(limit,4)))
    # print(f'This is the distance between {prev_pos}, {prev_z} and {new_pos}, {new_z}')

    if movement_ratio > limit:
        return 'reject'
    else:
        return 'accept'

# Standard method to construct a snake, raster or spiral scan

def construct_grid(
    initial,
    step_sizes,
    n_steps,
    style
):
    """
    Given an initial position, step sizes, and number of steps,
    construct a 2-dimensional list of scan x-y positions.
    """
    arr = []  # 2D array of coordinates

    if style == "spiral":
        # deal with the centre image immediately
        coord = initial
        arr.append([initial[0:2]])
        # for spiral, n_steps is the number of shells, and so only requires n_steps[0]
        for i in range(2, n_steps[0] + 1):
            arr.append([])  # Append new shell holder
            side_length = (2 * i) - 1

            # Iteratively generate the next location to append

            # Start coordinate of the shell
            # We create a copy of coord so that the new value of coord doesn't depend on itself
            # Otherwise we create a generator, not a tuple, which makes type checking angry
            last_coordinate = coord
            coord = (
                last_coordinate[0] + [-1, 1][0] * step_sizes[0],
                last_coordinate[1] + [-1, 1][1] * step_sizes[1],
            )
            for direction in ([1, 0], [0, -1], [-1, 0], [0, 1]):
                for _ in range(side_length - 1):
                    last_coordinate = coord
                    coord = (
                        last_coordinate[0] + direction[0] * step_sizes[0],
                        last_coordinate[1] + direction[1] * step_sizes[1],
                    )
                    arr[i - 1].append(coord)

    # If raster or snake
    else:
        for i in range(n_steps[0]):  # x axis
            arr.append([])
            for j in range(n_steps[1]):  # y axis
                # Create a coordinate tuple
                coord = (
                    initial[0] + [i, j][0] * step_sizes[0],
                    initial[1] + [i, j][1] * step_sizes[1],
                )
                # Append coordinate array to position grid
                arr[i].append(coord)

        # Style modifiers
        if style == "snake":
            # For each line (row) in the coordinate array
            for i, line in enumerate(arr):
                # If it's an odd row
                if i % 2 != 0:
                    # Reverse the list of coordinates
                    line.reverse()
    try:
        return np.concatenate(arr)
    except:
        return arr

def distance_to_site(current, next):
    current = np.array(current, dtype='float64') 
    next = np.array(next, dtype='float64')
    if (next[1] - current[1])**2 + (next[0] - current[0])**2 < 0:
        print(f'Negative distance between {next} and {current}')
    return np.sqrt((next[1] - current[1])**2 + (next[0] - current[0])**2, dtype='float64')

def choose_nearest_site(current, path):
    current = np.array(current)
    path = np.array(path)

    dists = [distance_to_site(current, pos) for pos in path]

    shortest_dist = np.min(dists)
    shortest_indices = np.where(dists == shortest_dist)
    if len(shortest_indices[0]) > 1:
        return np.min(path[shortest_indices],axis=0)
    else:
        return path[shortest_indices][0]


def choose_next_site(current, path):
    one_dir_path = []
    path = np.array(path)

    one_dir_path = path[(path[:,0] == current[0]) & (path[:,1] > current[1])]
    if not one_dir_path.size:
        one_dir_path = path[(path[:,0] == current[0]) & (path[:,1] < current[1])]
        print(1)
    if not one_dir_path.size:
        one_dir_path = path[(path[:,1] == current[1]) & (path[:,0] < current[0])]
        print(2)
    if not one_dir_path.size:
        one_dir_path = path[(path[:,1] == current[1]) & (path[:,0] > current[0])]
        print(3)
    try:
        return choose_nearest_site(current, one_dir_path)
    except:
        return choose_nearest_site(current, path)

def set_template(microscope, pos):
    microscope.move(pos)
    background = microscope.grab_image_array()
    background_LUV = cv2.cvtColor(background, cv2.COLOR_RGB2LUV)

    ch1 = (background_LUV.T[0]).flatten()
    ch2 = (background_LUV.T[1]).flatten()
    ch3 = (background_LUV.T[2]).flatten()

    points = np.array([np.asarray(ch1),np.asarray(ch2),np.asarray(ch3)]).T

    # we get the mean and standard deviation of values in each channel

    mu, std = np.apply_along_axis(norm.fit, 0, points)
    stats_list = np.vstack([mu, std])
    return stats_list

