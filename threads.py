import concurrent.futures
import logging
import queue
import random
import threading
import time
import pprint
import requests
from io import BytesIO
from PIL import Image, ExifTags
import os
import scatter
import cv2
import numpy as np
import json
import piexif
import piexif.helper
import pickle
import reconstruct_tiled_image
import matplotlib.pyplot as plt

import sys
sys.path.insert(0, r'C:\Users\Administrator\Source\openflexure-microscope-pyclient')
import openflexure_microscope_client

def pull_usercomment_dict(filepath):
    """
    Reads UserComment Exif data from a file, and returns the contained bytes as a dictionary.
    Args:
        filepath: Path to the Exif-containing file
    """
    try:
        exif_dict = piexif.load(filepath)
    except piexif._exceptions.InvalidImageDataError:
        logging.warning("Invalid data at {}. Skipping.".format(filepath))
        return None
    if "Exif" in exif_dict and piexif.ExifIFD.UserComment in exif_dict["Exif"]:
        try:
            m = piexif.load(filepath)
            return pickle.loads(m['Exif'][37510])
        except:
            try:
                return json.loads(exif_dict["Exif"][piexif.ExifIFD.UserComment].decode())
            except json.decoder.JSONDecodeError:
                logging.error(
                    f"Capture {filepath} has old, corrupt, or missing OpenFlexure metadata. Unable to reload to server."
                )
    else:
        return None

def download_from_id(microscope, id):
    link = microscope.base_uri
    r = requests.get(link + "/captures/" + id + "/download/my_image.jpg")
    r.raise_for_status()
    return r.content

def capture(queue, event, microscope, path, focused_path, true_path, stats_list, start_time, dont_check):
    """Pretend we're getting a number from the network."""
    
    while not event.is_set() or not path.empty():
        focused_path_list = list(focused_path.queue)
        true_path_list = list(true_path.queue)
        loc = path.get()[1]
        logging.info(f"Moving to {loc}")
        microscope.move(loc)
        sample_coverage = 10

        dx = 1800
        dy = 1400

        if len(focused_path_list) > 1:
            z_index = scatter.closest(loc, focused_path_list)
            microscope.move((loc[0], loc[1], focused_path_list[z_index][2]))

        # capture an image, convert it to LUV, then make lists of the 3 channels' values
        img = microscope.grab_image_array()

        img2 = cv2.cvtColor(img, cv2.COLOR_RGB2LUV)

        try:
            _, path_list = map(list, zip(*list(path.queue)))
        except:
            path_list = []

        # mask the image to only include pixels outside 5 stds of the mean of all three channels.
        # get the percent of pixels that are in the mask (assumed sample)

        img_mask = scatter.check_dist(img2, stats_list, 5)
        background_coverage = round(100*np.count_nonzero(img_mask)/img_mask.size, 1)

        if 100 - background_coverage < sample_coverage:
            category = 'background'
            dont_check.put([loc[0], loc[1]])
        else:
            # if not, it's sample. run an autofocus and use the updated height
            new_pos = [[microscope.position['x'], microscope.position['y'] + dy], [microscope.position['x'], microscope.position['y'] - dy], [microscope.position['x'] - dx, microscope.position['y']], [microscope.position['x'] + dx, microscope.position['y']]]
            for j, pos in enumerate(new_pos):
                if pos not in [sublist[:2] for sublist in true_path_list] and pos not in path_list and pos not in list(dont_check.queue):
                    path.put((j + 1, pos))

            category = 'sample'
            scatter.looping_autofocus(microscope)
            current_height = microscope.position['z']

            # if there have been successful autofocuses in this scan, find the closest one in x-y
            # test if the change in z between them exceeds a ratio (indicating a failed autofocus)

            if len(focused_path_list) > 0:
                nearest_focused_site = focused_path_list[scatter.closest(loc, focused_path_list)]
                result = scatter.limit_focus_change(nearest_focused_site[0:2], nearest_focused_site[-1], loc[0:2], current_height, 0.8)

            # if there haven't been any previous autofocuses, we have to assume this one worked
            else:
                result = 'accept'

            # if the autofocus worked, take a new, more focused image. add the current position to the list of successful locations
            if result == 'accept':
                loc = list(microscope.position.values())
                focused_path.put(loc)
            else:

            # if the autofocus was rejected, we return to the height of the closest successful autofocus. not perfect, but better than wandering out of focus
                print('Using a previous focus position. Options are {0}, we chose {1}'.format(focused_path_list, nearest_focused_site))
                microscope.move((microscope.position['x'],microscope.position['y'], focused_path[z_index][2]))

        logging.info(f'Amount of background in image is {background_coverage}. That means this is {category}')

        params = {
            "use_video_port": False,
            "bayer": False,
            "temporary" : False,
            "filename": f"{start_time}/{loc[0]}_{loc[1]}",
            'tags': [category]
        }

        # add the current position to the list of all positions visited
        true_path.put(loc)

        m = microscope.capture_image_to_disk(params)
        logging.info("Producer got message: %s", m['id'])
        queue.put(m['id'])

    logging.info("Producer received event. Exiting")

def transfer(queue, event, microscope, path, folder_path, tiling_list):
    """Pretend we're saving a number in the database."""
    while not event.is_set() or not queue.empty() or not path.empty():
        message = queue.get()
        logging.info(
            "Consumer storing message: %s (size=%d)", message, queue.qsize()
        )
        im = download_from_id(microscope, message)
        im = Image.open(BytesIO(im))

        data = microscope.get_capture_metadata(message)['metadata']
        bytes_data = pickle.dumps(data)

        meta_dict = piexif.load(r'C:\Users\Administrator\Source\background-detect\scans\0\img_0.jpg')

        meta_dict["Exif"] = {
                piexif.ExifIFD.UserComment : bytes_data
            }

        exif_bytes = piexif.dump(meta_dict)

        im = im.save(os.path.join(folder_path, data['image']['name']), exif = exif_bytes)

        time.sleep(1)

        tiling_list.put(data['image']['name'])

    logging.info("Consumer received event. Exiting")

def tile(folder_path, event, tiling_list):
    last_tile_size = 0
    img_list = []
    while not event.is_set() or not tiling_list.empty() or not path.empty() or not pipeline.empty():
        new_tile = tiling_list.get()
        img_list.append(new_tile)
        logging.info('Starting to tile')
        data = pull_usercomment_dict(os.path.join(folder_path, img_list[0]))
        camera_to_sample = data['instrument']['settings']['extensions']['org.openflexure.camera_stage_mapping']['image_to_stage_displacement']
        downsample = 15
        resize = 1/4
        unknown_images = "warn"
        fractional_overlap= 0.1
        img_limit = 0
        try:
            pairs, pair_data, pixel_positions, tiles, unknown_images, camera_to_sample, resize, fractional_overlap, starting_pixel_positions, scan_centre = reconstruct_tiled_image.reconstruct_tiled_image(folder_path, img_list,
                                                camera_to_sample=camera_to_sample,
                                                downsample=downsample, resize = resize, unknown_images = unknown_images, fractional_overlap= fractional_overlap, img_limit = img_limit)
            x_thresh = 0.9998
            y_thresh = 170
            tiles, positions, csm, scan_centre = reconstruct_tiled_image.stitch_from_thresholds(folder_path, pairs, pair_data, pixel_positions, tiles, unknown_images, camera_to_sample, resize, fractional_overlap, starting_pixel_positions, scan_centre, x_thresh, y_thresh, img_limit, downsample)
            reconstruction = reconstruct_tiled_image.generate_stitched_image(tiles, positions, resize, downsample, csm, scan_centre)
            stitched_image = reconstruction['stitched_image']
            cv2.imwrite(os.path.join(folder_path, "stitched.jpg"), cv2.cvtColor(stitched_image[:,:,::-1], cv2.COLOR_BGR2RGB))
        except BaseException as e:
            logging.error(f'Failed to stitch {len(img_list)} images because {str(e)}')

if __name__ == "__main__":
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")

    microscope = openflexure_microscope_client.MicroscopeClient("10.120.33.184")    

    stats_list = scatter.set_template(microscope, ((-11945, -28211, -900)))

    path = queue.PriorityQueue(maxsize=0)
    pipeline = queue.Queue(maxsize=0)
    focused_path = queue.Queue(maxsize=0)
    true_path = queue.Queue(maxsize=0)
    event = threading.Event()
    tiling_list = queue.Queue(maxsize=0)
    dont_check = queue.Queue(maxsize=0)

    # if necessary, move to the starting point for your scan
    microscope.move((-8297, -31211, -900))

    scatter.looping_autofocus(microscope)

    dx = 2200
    dy = 1500
    sample_coverage = 10

    start_time = time.strftime("%H_%M_%S-%d_%m_%Y")

    path.put((1,list(microscope.position.values())))

    folder_path = 'scans'
    
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

    i = 0
    while os.path.exists(os.path.join(folder_path, str(i))):
        i += 1

    folder_path = os.path.join(folder_path, str(i))
    os.makedirs(folder_path)

    with concurrent.futures.ThreadPoolExecutor(max_workers=3) as executor:
        executor.submit(capture, pipeline, event, microscope, path, focused_path, true_path, stats_list, start_time, dont_check)
        executor.submit(transfer, pipeline, event, microscope, path, folder_path, tiling_list)
        

        while not tiling_list.qsize() > 0:
            time.sleep(1)

        executor.submit(tile, folder_path, event, tiling_list)
        logging.info("Main: about to set event")
        event.set()

